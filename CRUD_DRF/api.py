from rest_framework import routers
from rest_app.api_views import *


router = routers.DefaultRouter()
router.register('friend', FriendViewSet)
router.register('belonging', BelongingViewSet)
router.register('borrowings', BorrowedViewSet)